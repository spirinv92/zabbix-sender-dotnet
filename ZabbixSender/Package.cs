﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ZabbixSender.Entities;

namespace ZabbixSender
{
    public class Package
    {

        public byte[] Create(Metric[] metrics)
        {
            var package = new List<byte>();

            var header = this.getHeader();
            var data = this.getData(metrics);
            var len = this.getLength(data);

            package.AddRange(header);
            package.AddRange(len);
            package.AddRange(data);

            return package.ToArray();
        }

        /// <summary>
        /// Constant value.
        /// ASCII - ZBXD\u0001.
        /// </summary>
        /// <returns></returns>
        private byte[] getHeader() => new byte[] { 0x5a, 0x42, 0x58, 0x44, 0x01 };

        /// <summary>
        /// Размер отправляемого пакета.
        /// Под блок длины выделяется 8 байт.
        /// Учитывает только блок data (header и length игнорируются).
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private byte[] getLength(byte[] data) => BitConverter.GetBytes(Convert.ToInt64(data.Length));

        private byte[] getData(Metric[] metrics) =>
            Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(new SenderRequest(metrics)));
    }
}
