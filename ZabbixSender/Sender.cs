﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZabbixSender.Entities;

namespace ZabbixSender
{
    public class Sender: IDisposable
    {
        private Socket socket;
        private readonly Package package;
        private readonly int receiveTimeout;
        private readonly int sendTimeout;

        public Sender(int receiveTimeout = 1000, int sendTimeout = 2000 )
        {
            this.receiveTimeout = receiveTimeout;
            this.sendTimeout = sendTimeout;
            this.package = new Package();
        }

        #region Main methods

        public bool Connect(string address, int port)
        {
            this.socket = this.initSocket();

            try
            {
                this.socket.Connect(IPAddress.Parse(address), port);
            }
            catch
            {
                return false;
            }

            return this.socket.Connected;

        }

        public async Task<bool> ConnectAsync(string ipAddress, int port)
        {
            this.socket = this.initSocket();

            try
            {
                await this.socket.ConnectAsync(IPAddress.Parse(ipAddress), port).ConfigureAwait(false);
            }
            catch
            {
                return false;
            }

            return this.socket.Connected;
        }

        public bool Send(Metric[] metrics)
        {
            bool result;

            try
            {
                this.send(metrics);
                var response = this.receive();
                result = this.isSuccess(response);
            }
            catch
            {
                return false;
            }

            return result;
        }

        public async Task<bool> SendAsync(Metric[] metrics)
        {
            bool result;

            try
            {
                await this.sendAsync(metrics, this.sendTimeout).ConfigureAwait(false);
                var response = await this.receiveAsync(this.receiveTimeout).ConfigureAwait(false);
                result = this.isSuccess(response);
            }
            catch
            {
                return false;
            }

            return result;

        }

        public void Disconnect()
        {
            if (this.socket == null) return;

            try
            {
                if (this.socket.Connected)
                {
                    this.socket.Shutdown(SocketShutdown.Both);
                    this.socket.Disconnect(false);
                }
            }
            finally
            {
                this.socket.Dispose();
            }
        }

        public Task DisconnectAsync()
        {
            var tcs = new TaskCompletionSource<bool>(this);
            this.socket.BeginDisconnect(false, iar =>
            {
                try
                {
                    this.socket.Shutdown(SocketShutdown.Both);
                    this.socket.EndDisconnect(iar);
                    this.socket.Dispose();
                    tcs.TrySetResult(true);
                }
                catch (Exception ex)
                {
                    tcs.TrySetException(ex);
                }
            }, tcs);

            return tcs.Task;
        }

        #endregion

        #region Private Sync methods

        private void send(Metric[] metrics)
        {
            this.socket.Send(package.Create(metrics));
        }

        private SenderResponse receive()
        {
            var buffer = new byte[256];
            try
            {
                this.socket.Receive(buffer);
                var output = Encoding.UTF8.GetString(buffer);
                return JsonConvert.DeserializeObject<SenderResponse>(output.Substring(output.IndexOf('{')));
            }
            catch
            {
                return null;
            }

        }

        private Socket initSocket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            {
                SendTimeout = this.sendTimeout,
                ReceiveTimeout = this.receiveTimeout
            };
        }

        private bool isSuccess(SenderResponse response)
        {
            return response?.Info.Contains("failed: 0") ?? false;
        }

        #endregion

        #region Private Async methods

        private async Task sendAsync(Metric[] metrics, int timeout)
        {
            var sendTask = this.socket.SendAsync(this.package.Create(metrics), SocketFlags.None);
            if (sendTask != await Task.WhenAny(sendTask, Task.Delay(timeout)))
                throw new TimeoutException("Send async timeout");
        }

        private async Task<SenderResponse> receiveAsync(int timeout)
        {
            var buffer = new byte[256];
            var receiveTask = this.socket.ReceiveAsync(buffer, SocketFlags.None);

            if (receiveTask != await Task.WhenAny(receiveTask, Task.Delay(timeout)))
                throw new TimeoutException("Receive async timeout");

            await receiveTask;
            var output = Encoding.UTF8.GetString(buffer);
            return JsonConvert.DeserializeObject<SenderResponse>(output.Substring(output.IndexOf('{')));

        }

        #endregion

        #region Disposing
        public void Dispose()
        {
            this.Disconnect();
        }
        #endregion

    }
}
