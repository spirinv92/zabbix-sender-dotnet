﻿using Newtonsoft.Json;

namespace ZabbixSender.Entities
{
    public class SenderRequest
    {
        [JsonProperty("request")]
        public string Request { get; }

        [JsonProperty("data")]
        public Metric[] Data { get; }

        public SenderRequest(Metric[] metrics)
        {
            this.Data = metrics;
            this.Request = "sender data";
        }
    }
}
