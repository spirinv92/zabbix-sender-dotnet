﻿using Newtonsoft.Json;

namespace ZabbixSender.Entities
{
    public class Metric
    {
        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
