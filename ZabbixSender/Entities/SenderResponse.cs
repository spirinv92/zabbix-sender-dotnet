﻿using Newtonsoft.Json;

namespace ZabbixSender.Entities
{
    public class SenderResponse
    {
        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }
    }
}
