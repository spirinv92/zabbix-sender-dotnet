﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZabbixSender;
using ZabbixSender.Entities;

namespace ConsoleTest
{
    class Program
    {

        static async Task Main(string[] args)
        {
            var metrics = new[]
            {
                    new Metric
                    {
                        Host = "7701-0401",
                        Key = "test_key",
                        Value = "test_item"
                    }
                };

            var sender = new Sender();

            for (int i = 0; i < 25; i++)
            {
                await sender.ConnectAsync("10.7.10.149", 10051);
                Console.WriteLine(await sender.SendAsync(metrics));
                await sender.DisconnectAsync();

            }

            Console.ReadKey();
        }
    }
}
