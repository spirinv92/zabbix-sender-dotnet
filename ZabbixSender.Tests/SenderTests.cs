﻿using System.Threading.Tasks;
using Xunit;
using ZabbixSender.Entities;

namespace ZabbixSender.Tests
{
    public class SenderTests
    {
        private const string Address = "10.7.8.157";
        private const string WrongAddress = "127.0.0.1";
        private const int Port = 10051;

        private readonly Metric[] successMetrics = new[]
        {
            new Metric
            {
                Host = "test",
                Key = "Valdator_version1",
                Value = "test"
            }
        };

        private readonly Metric[] wrongMetrics = new[]
        {
            new Metric
            {
                Host = "unknown_host",
                Key = "WRONG_KEY",
                Value = "WRONG_DATA"
            }
        };


        [Fact]
        public void Connect_Success()
        {
            using (var sender = new Sender())
            {
                var result = sender.Connect(Address, Port);
                Assert.True(result);
            }
        }

        [Fact]
        public async Task ConnectAsync_Success()
        {
            using (var sender = new Sender())
            {
                var result = await sender.ConnectAsync(Address, Port);
                Assert.True(result);
            }
        }


        [Fact]
        public void Connect_Failed()
        {
            using (var sender = new Sender())
            {
                var result = sender.Connect(WrongAddress, Port);
                Assert.False(result);
            }
        }


        [Fact]
        public async Task ConnectAsync_Failed()
        {
            using (var sender = new Sender())
            {
                var result = await sender.ConnectAsync(WrongAddress, Port);
                Assert.False(result);
            }
        }

        [Fact]
        public void Send_Success()
        {
            using (var sender = new Sender())
            {
                sender.Connect(Address, Port);
                var result = sender.Send(successMetrics);
                Assert.True(result);
            }
        }

        [Fact]
        public async Task SendAsync_Success()
        {
            using (var sender = new Sender())
            {
                await sender.ConnectAsync(Address, Port);
                var result = await sender.SendAsync(successMetrics);
                Assert.True(result);
            }
        }


        [Fact]
        public void Send_WrongMetric_Failed()
        {
            using (var sender = new Sender())
            {
                sender.Connect(Address, Port);
                var result = sender.Send(wrongMetrics);
                Assert.False(result);
            }
        }

        [Fact]
        public void Send_UnavailableServer_Failed()
        {
            using (var sender = new Sender())
            {
                sender.Connect(WrongAddress, Port);
                var result = sender.Send(successMetrics);
                Assert.False(result);
            }
        }


        [Fact]
        public async Task SendAsync_WrongMetric_Failed()
        {
            using (var sender = new Sender())
            {
                await sender.ConnectAsync(Address, Port);
                var result = await sender.SendAsync(wrongMetrics);
                Assert.False(result);
            }
        }

        [Fact]
        public async Task SendAsync_UnavailableServer_Failed()
        {
            using (var sender = new Sender())
            {
                await sender.ConnectAsync(WrongAddress, Port);
                var result = await sender.SendAsync(successMetrics);
                Assert.False(result);
            }
        }


    }
}
